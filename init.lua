local version = "2.3.5"
local srcpath = minetest.get_modpath("magic_compass") .. "/src"

-- TODO: remove after https://github.com/minetest/modtools/issues/2
local S = minetest.get_translator("magic_compass")

S("Magic Compass")
S("Item to teleport into server-side defined locations")

magic_compass = {}

dofile(srcpath .. "/_load.lua")
dofile(srcpath .. "/callbacks.lua")
dofile(srcpath .. "/deserializer.lua")
dofile(srcpath .. "/items.lua")
dofile(srcpath .. "/utils.lua")
dofile(srcpath .. "/GUI/gui_compass.lua")

minetest.log("action", "[MAGIC COMPASS] Mod initialised, running version " .. version)
