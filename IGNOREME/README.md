
**EDITING THIS FOLDER IS USELESS, EDIT THE ONE CALLED magic_compass IN THE WORLD FOLDER INSTEAD**

The first time the world is launched with `magic_compass` on, this folder is copy-pasted into the world folder (with the sole exception of this file). The purpose of this folder is to provide all the menu assets and a few examples by default. This helps modders understand how to implement their locations more easily.
