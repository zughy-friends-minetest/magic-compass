local S = minetest.get_translator("magic_compass")

local function load_folder()
  local wrld_dir = minetest.get_worldpath() .. "/magic_compass"
  local content = minetest.get_dir_list(wrld_dir)

  local modpath = minetest.get_modpath("magic_compass")

  -- se la cartella della bussola non esiste/è vuota, copio la cartella base `IGNOREME`
  if not next(content) then
    local src_dir = modpath .. "/IGNOREME"
    minetest.cpdir(src_dir, wrld_dir)
    os.remove(wrld_dir .. "/README.md")
  end

  --v------------------ LEGACY UPDATE, to remove in 4.0 -------------------v
  local txtr_dir = modpath .. "/textures/locations"
  local i18n_dir = modpath .. "/locale/locations"

  if next(minetest.get_dir_list(txtr_dir)) then
    minetest.rmdir(i18n_dir)
    minetest.rmdir(modpath .. "/textures/menu")
    minetest.rmdir(txtr_dir)
  end

  local close_icon = io.open(wrld_dir .. "/menu/magiccompass_gui_close.png", "r")

  if not close_icon then
    local close_icon_orig = io.open(modpath .. "/IGNOREME/menu/magiccompass_gui_close.png")
    minetest.safe_file_write(wrld_dir .. "/menu/magiccompass_gui_close.png", close_icon_orig:read("*a"))
  else
    close_icon:close()
  end

  for _, dir_name in ipairs(minetest.get_dir_list(modpath .. "/locale", true)) do
    if dir_name == "locations" then
      minetest.rmdir(i18n_dir, true)
    end
  end
  --^------------------ LEGACY UPDATE, to remove in 4.0 -------------------^

  -- carico quel che posso dalla cartella del mondo (non davvero media dinamici,
  -- dato che vengon eseguiti all'avvio del server)
  local function iterate_dirs(dir)
    for _, f_name in pairs(minetest.get_dir_list(dir, false)) do
      minetest.dynamic_add_media({filepath = dir .. "/" .. f_name}, function(name) end)
    end
    for _, subdir in pairs(minetest.get_dir_list(dir, true)) do
      iterate_dirs(dir .. "/" .. subdir)
    end
  end

  -- TEMP MT 5.9: per ora non si possono aggiungere contenuti dinamici all'avvio
  -- del server. Poi rimuovi anche 2° param da dynamic_add_media qui in alto
  minetest.after(0.1, function()
    iterate_dirs(wrld_dir .. "/icons")
    iterate_dirs(wrld_dir .. "/locale")
    iterate_dirs(wrld_dir .. "/menu")
  end)
end

load_folder()

dofile(minetest.get_worldpath() .. "/magic_compass/SETTINGS.lua")





----------------------------------------------
------------------AUDIO_LIB-------------------
----------------------------------------------

if not minetest.get_modpath("audio_lib") then return end

audio_lib.register_sound("sfx", "magiccompass_teleport", S("Teleport beeps"))
audio_lib.register_sound("sfx", "magiccompass_teleport_deny", S("Action forbidden"))